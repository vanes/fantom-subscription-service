<?php 
include("auth.php"); 
include("header.php"); 

$www_user_id = $_SESSION['UID'];
echo "<div class=\"row\"><div class=\"col-md-12\">";
echo "<div class=\"card\"><div class=\"card-header\">";
echo "<h4 class=\"card-title\">My subscriptions</h4></div>";
echo "<div class=\"card-body\"><div class=\"table-responsive\">";
echo "<table class=\"table tablesorter\" id=\"\">";
echo "<thead class=\"text-primary\"><tr><th>Service</th><th>Expirations date</th>";
echo "<th>Cost</th><th class=\"text-center\">Currency</th><th></th></tr></thead><tbody>";

$result = $db->query("SELECT sb.id, sb.end_dt, s.name AS service, s.cost 
                        FROM subscriptions sb 
                   LEFT JOIN services s ON sb.service = s.id 
                       WHERE sb.www_user='$www_user_id'
                         AND sysdate() < sb.end_dt");  

while($row = $result->fetch(PDO::FETCH_ASSOC)) 
{ 
	$id = $row['id'];
	$service = $row['service'];
	$end_dt = $row['end_dt'];
	$cost = $row['cost'];
	echo "<tr><td>$service</td><td>$end_dt</td><td>$cost</td><td class=\"text-center\">ETH</td>
	      <td><button id=\"$id\" class=\"cancel-sub-button\" name=\"id\" value=\"$id\">Cancel</button>
	      </td></tr>";
}
echo "</tbody></table></div></div></div></div></div>";
include("footer.php");
?>

