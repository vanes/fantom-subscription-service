<?php 
include("auth.php"); 
include("header.php"); 

$www_user_id = $_SESSION['UID'];
echo "<div class=\"row\"><div class=\"col-md-12\">";
echo "<div class=\"card\"><div class=\"card-header\">";
echo "<h4 class=\"card-title\">Available subscriptions</h4></div>";
echo "<div class=\"card-body\"><div class=\"table-responsive\">";
echo "<table class=\"table tablesorter\" id=\"\">";
echo "<thead class=\"text-primary\"><tr><th>Service</th><th>Category</th><th>Company</th>";
echo "<th>Cost</th><th class=\"text-center\">Currency</th><th></th></tr></thead><tbody>";

$result = $db->query("SELECT s.id, s.name, sg.name AS sg_name, s.cost, u.name as company
                        FROM services s
                   LEFT JOIN service_groups sg ON s.service_group_id = sg.service_group_id 
                   LEFT JOIN www_users u ON sg.owner = u.id
                     ");  

while($row = $result->fetch(PDO::FETCH_ASSOC)) 
{ 
	$id = $row['id'];
	$service = $row['name'];
	$sg_name = $row['sg_name'];
	$cost = $row['cost'];
	$company = $row['company'];
	echo "<tr><td>$service</td><td>$sg_name</td><td>$company</td><td>$cost</td>
	      <td class=\"text-center\">ETH</td>
	      <td><button id=\"$id\" class=\"add-sub-button\" name=\"id\" value=\"$id\">Add</button>
	      </td></tr>";
}
echo "</tbody></table></div></div></div></div></div>";

include("footer.php");

?>


