<?php 
include("auth.php"); 
include("header.php"); 

$www_user_id = $_SESSION['UID'];
echo "<div class=\"row\"><div class=\"col-md-8\">";
echo "<div class=\"card\"><div class=\"card-header\">";
echo "<h4 class=\"card-title\">My services</h4></div>";
echo "<div class=\"card-body\"><div class=\"table-responsive\">";
echo "<table class=\"table tablesorter\" id=\"\">";
echo "<thead class=\"text-primary\"><tr><th>Service</th><th>Group</th><th>Cost</th>";
echo "<th class=\"text-center\">Currency</th></tr></thead><tbody>";

$result = $db->query("SELECT s.name, sg.name AS sg_name, s.cost
                        FROM services s
                   LEFT JOIN service_groups sg ON s.service_group_id = sg.service_group_id 
                       WHERE sg.owner = '$www_user_id'
                     ");  

while($row = $result->fetch(PDO::FETCH_ASSOC)) 
{ 
	$service = $row['name'];
	$sg_name = $row['sg_name'];
	$cost = $row['cost'];
	echo "<tr><td>$service</td><td>$sg_name</td><td>$cost</td>
	      <td class=\"text-center\">FTM</td></tr>";
}
echo "</tbody></table></div></div></div></div>";

echo "<div class=\"col-md-4\">";
echo "<div class=\"card\"><div class=\"card-header\">";
echo "<h4 class=\"card-title\">Service groups</h4></div>";
echo "<div class=\"card-body\"><div class=\"table-responsive\">";
echo "<table class=\"table tablesorter\" id=\"\">";
echo "<thead class=\"text-primary\"><tr>";
echo "<th class=\"text-center\">Groups</th></tr></thead><tbody>";

$result = $db->query("SELECT sg.name 
                        FROM service_groups sg
                       WHERE sg.owner = '$www_user_id'
                     ");  

while($row = $result->fetch(PDO::FETCH_ASSOC)) 
{ 
	$service_group = $row['name'];
	echo "<tr><td class=\"text-center\">$service_group</td></tr>";
}
echo "</tbody></table></div></div></div></div>";

echo "</div>";

include("footer.php");

?>


