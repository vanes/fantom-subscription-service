<?php 
include("auth.php"); 

function add_month($time, $num=1) {
    $d=date('d',$time);  // день
    $m=date('m',$time);  // месяц
    $y=date('Y',$time);  // год
 
    // Прибавить месяц(ы)
    $m+=$num;
    if ($m>12) {
        $y+=floor($m/12);
        $m=($m%12);
        // Дополнительная проверка на декабрь
        if (!$m) {
            $m=12;
            $y--;
        }
    }
 
    // Это последний день месяца?
    if ($d==date('t',$time)) {
        $d=31;
    }
    // Открутить дату, пока она не станет корректной
    while(true) {
        if (checkdate($m,$d,$y)){
            break;
        }
        $d--;
    }
    // Вернуть новую дату в TIMESTAMP
    return mktime(0,0,0,$m,$d,$y);
}

$www_user_id = $_SESSION['UID'];
$service_id = $_POST['id'];
$create_time = date("Y-m-d H:i:s");
$end_dt = add_month($create_time, 1);
$result = $db->query("INSERT INTO subscriptions (www_user, service, create_dt, start_dt, end_dt) 
                           VALUES ('$www_user_id', '$service_id', '$create_time', '$create_time', '$end_dt')");  


?>
