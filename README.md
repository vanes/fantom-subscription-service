# fantom-subscription-service
Crypto enables push transactions (as opposed to pull transactions with credit cards), and this model has made it difficult for merchants to accept recurring payments. If you are subscribing to various services in FTM, I can to receive a monthly list of subscriptions so that you can pay for them all in one place. This project is not simply subscription service - it is billing system with cryptocurrency. Now service support only FTM/ETH cryptocurrency, but system is easy to scale for support another cryptocurrency and payments schemes (for example, periodic/non-perodic payments). 


#implementation
The project implemented as web-service. In this stage project support two categories of users: abonents and companies, which give them access for service (for example, Amazon). User can subscribe for service and cancel subscription. With subscription user init transaction to send FTM tokens for Company, which provided service. User can cancel subscription and get cashback for time, which he did't use the service (for example, cost of subscription is 30tokens, but user canceled subscription on second day, 28tokens company will return). Moreover, service provide notification system (for example, about recharge or about errors in the system). The Company can watch statistics about their subscribers, balance history and services popularity.

#how to run the demo
Demo available by link: http://fantom.ibisolutions.ru/

Login/pass 1
- login: if1242@yandex.ru
- password: 123

Login/pass 2
-login: ivfedorov1242@gmail.com
-password: 123


